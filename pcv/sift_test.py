import cv2
import numpy as np
import scipy.misc
from scipy.ndimage import filters
import matplotlib.pyplot as plt

im = scipy.misc.ascent().astype(dtype=np.uint8)
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
sift = cv2.xfeatures2d.SIFT_create()
kp = sift.detect(im, None)
outI = np.zeros(im.shape)
img = cv2.drawKeypoints(im, kp, outI)
plt.imshow(img)
plt.show()