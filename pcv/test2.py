import scipy.misc
import numpy as np
from scipy.ndimage import filters
import matplotlib.pyplot as plt

sigma = 1
im = scipy.misc.ascent()

imx = np.zeros(im.shape)
filters.gaussian_filter(im, (sigma,sigma), (1,0), imx)


plt.gray()
plt.subplot(121)
plt.imshow(im)
plt.subplot(122)
plt.imshow(imx)
plt.show()