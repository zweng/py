import scipy.misc
import numpy as np
from scipy.ndimage import filters
import matplotlib.pyplot as plt


def compute_harris_response(im, sigma=3):
    """ Compute the Harris corner detector response function
	for each pixel in a graylevel image. """
    # derivatives
    imx = np.zeros(im.shape)
    filters.gaussian_filter(im, (sigma, sigma), (0, 1), imx)
    imy = np.zeros(im.shape)
    filters.gaussian_filter(im, (sigma, sigma), (1, 0), imy)
    # compute components of the Harris matrix
    Wxx = filters.gaussian_filter(imx * imx, sigma)
    Wxy = filters.gaussian_filter(imx * imy, sigma)
    Wyy = filters.gaussian_filter(imy * imy, sigma)
    # determinant and trace
    Wdet = Wxx * Wyy - Wxy**2
    Wtr = Wxx + Wyy
    return Wdet / Wtr


ascent = scipy.misc.ascent()
plt.gray()
plt.subplot(121)
plt.imshow(ascent)
y = compute_harris_response(ascent)
plt.subplot(122)
plt.imshow(y)
plt.show()